#!/bin/bash
#/bin/sh -c /usr/bin/php-fpm
service php7.2-fpm start
cd /application
composer install -vvv

php /application/artisan cache:clear
php /application/artisan config:cache

php /application/artisan migrate --force
php /application/artisan db:seed --force

cronfile=/etc/cron.d/cron
cat /dev/null > $cronfile
for cronvar in ${!CRON_*}; do
	cronvalue=${!cronvar}
	echo "Installing $cronvar"
done

echo "$cronvalue >> /var/log/cron.log 2>&1" | tr -d '"' >> $cronfile
echo >> $cronfile # Newline is required


mkfifo /var/log/cron.log
chown -R www-data:www-data /application
service cron restart
tail -f /var/log/cron.log
