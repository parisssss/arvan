<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (\App\Models\User::where('name', '=', 'admin')->first() == null) {
            \App\Models\User::create([
                'name' => 'admin',
                'email' => 'parisssss.e@gmail.com',
                'password' => bcrypt('password')
            ]);
        }
    }
}
