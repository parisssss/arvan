<?php

use Illuminate\Database\Seeder;
use App\RecordData\DnsTypes;
use App\Models\DnsType;

class DnsTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $constants = new ReflectionClass(DnsTypes::class);

        foreach ($constants->getConstants() as $type) {
            if (DnsType::where('name', '=', $type)->first() == null) {
                DnsType::create(['name' => $type]);
            }
        }
    }
}
