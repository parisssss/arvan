<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraFieldsToDnsRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dns_records', function (Blueprint $table) {
            $table->integer('ttl')->nullable();
            $table->string('class')->nullable();
            $table->string('comment')->nullable();
            $table->renameColumn('title', 'name');
            $table->renameColumn('value', 'data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dns_records', function (Blueprint $table) {
            $table->dropColumn('ttl');
            $table->dropColumn('class');
            $table->dropColumn('comment');

            $table->renameColumn('name', 'title');
            $table->renameColumn('data', 'value');
        });
    }
}
