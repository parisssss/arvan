<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ParseZoneEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $content;
    public $domain;

    public function __construct($content, $domain)
    {
        $this->content = $content;
        $this->domain = $domain;
    }
}
