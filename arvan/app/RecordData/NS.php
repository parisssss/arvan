<?php

namespace App\RecordData;

class NS implements RecordDataInterface
{
    const TYPE = 'NS';

    protected $target;

    public function extractInfo(string $info)
    {
       $this->target = $info;
    }

    public function toJson()
    {
        return json_encode(['target' => $this->target]);
    }
}
