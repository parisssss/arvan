<?php

namespace App\RecordData;

class MX implements RecordDataInterface
{
    const TYPE = 'MX';

    private $preference;

    private $exchange;

    public function extractInfo(string $info)
    {
        list($this->preference, $this->exchange) = explode(' ', $info, 2);
    }

    public function toJson()
    {
        return json_encode([
            'exchange' => $this->exchange,
            'preferences' => $this->preference
        ]);
    }
}
