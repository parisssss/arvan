<?php

namespace App\RecordData;

/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/3/19
 * Time: 3:32 PM
 */
interface RecordDataInterface
{
    public function toJson();

    public function extractInfo(string $info);
}