<?php

namespace App\RecordData;

class A implements RecordDataInterface
{

    const TYPE = 'A';

    protected $address;

    public function toJson()
    {
        return json_encode(['address' => $this->address]);
    }

    public function extractInfo(string $info)
    {
        $this->address = $info;
    }
}
