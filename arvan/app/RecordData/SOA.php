<?php

/*
 * This file is part of Badcow DNS Library.
 *
 * (c) Samuel Williams <sam@badcow.co>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\RecordData;

class SOA implements RecordDataInterface
{

    const TYPE = 'SOA';

    private $mname;

    private $rname;

    private $serial;

    private $refresh;

    private $retry;

    private $expire;

    private $ttl;

    public function setExpire($expire)
    {
        $this->expire = (int)$expire;
    }

    public function setTtl($ttl)
    {
        $this->ttl = (int)$ttl;
    }

    public function setMname($mname)
    {
        $this->mname = $mname;
    }

    public function setRefresh($refresh)
    {
        $this->refresh = (int)$refresh;
    }

    public function setRetry($retry)
    {
        $this->retry = (int)$retry;
    }

    public function setRname($rname)
    {
        $this->rname = $rname;
    }

    public function setSerial($serial)
    {
        $this->serial = (int)$serial;
    }

    public function toJson()
    {
        return json_encode([
            'mname' => $this->mname,
            'rname' => $this->rname,
            'serial' => $this->serial,
            'refresh' => $this->refresh,
            'retry' => $this->retry,
            'expire' => $this->expire,
            'ttl' => $this->ttl,
        ]);
    }

    public function extractInfo(string $info)
    {
        list($this->mname, $this->rname) = explode(' ', str_replace('(', '', $info), 2);
    }

    public function setData($info)
    {
        $this->mname = $info->mname;
        $this->rname = $info->rname;
        $this->serial = $info->serial;
        $this->refresh = $info->refresh;
        $this->retry = $info->retry;
        $this->expire = $info->expire;
        $this->ttl = $info->ttl;
    }
}
