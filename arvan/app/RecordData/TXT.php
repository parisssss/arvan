<?php

namespace App\RecordData;

class TXT implements RecordDataInterface
{
    const TYPE = 'TXT';

    private $text;

    public function extractInfo(string $info)
    {
        $this->text = $info;
    }

    public function toJson()
    {
        return json_encode(['text' => $this->text]);
    }
}
