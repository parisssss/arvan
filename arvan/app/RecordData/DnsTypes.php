<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/4/19
 * Time: 5:36 PM
 */

namespace App\RecordData;


class DnsTypes
{
    public static $names = [
        self::A, self::AAAA, self::MX, self::NS, self::TXT, self::IN, self::SOA
    ];

    const A = 'A';
    const AAAA = 'AAAA';
    const MX = 'MX';
    const NS = 'NS';
    const TXT = 'TXT';
    const IN = 'IN';
    const SOA = 'SOA';

    public static function isValid($type)
    {
        return in_array($type, self::$names);
    }

    public static function factory($type)
    {
        $namespace = "\\App\\RecordData\\";
        if (!class_exists($namespace . $type)) {
            throw new \InvalidArgumentException($type . 'is not implemented');
        }

        $className = $namespace . $type;
        return new $className();
    }

    public static function generateData($type, $data)
    {
        $parser = self::factory($type);
        $parser->extractInfo($data);
        return $parser->toJson();
    }
}