<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DnsType extends Model
{
    protected $fillable = ['name'];
}
