<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DnsRecord extends Model
{
    protected $fillable = [
        'type_id',
        'domain_id',
        'name',
        'data',
        'comment',
        'class',
        'ttl'
    ];

    public function type()
    {
        return $this->belongsTo(DnsType::class, 'type_id');
    }

    public function domain()
    {
        return $this->belongsTo(Domain::class, 'domain_id');
    }
}
