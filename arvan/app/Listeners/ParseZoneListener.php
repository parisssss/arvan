<?php

namespace App\Listeners;

use App\Events\ParseZoneEvent;
use App\Models\DnsRecord;
use App\Models\Domain;
use App\RecordData\DnsTypes;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class ParseZoneListener
{

    private $previousName;
    private $isMultipart;
    private $domain;
    private $ttl;

    public function handle(ParseZoneEvent $event)
    {
        /**
         * @todo this part should be in repository
         */
        $this->domain = Domain::where('name', $event->domain)->first();

        if (!$this->domain) {
            $this->domain = new Domain();
            $this->domain->name = $event->domain;
            /**
             * @todo it should be fetched from responsible user
             */
            $this->domain->user_id=1;
            $this->domain->save();
        }

        collect(explode("\n", $event->content))->filter(function ($line) {
            if (strpos($line, ';') !== 0 && strpos($line, "\r") !== 0) {
                return true;
            }
        })->each(function ($line) {
            $this->processLine(preg_replace('/\s+/', ' ', $line));
        });
    }

    public function processLine($line)
    {
        $params = array_combine(['name', 'type', 'data'], explode(' ', $line, 3));

        $name = str_replace('@', $this->domain->name, $params['name']);
        if ($name == '') {
            $name = $this->previousName;
        } else {
            $this->previousName = $name;
        }

        $type = $params['type'];

        if (!DnsTypes::isValid($type)) {

            //check if it's proportion of a multipart resource
            if (!$this->isMultipart) {
                Log::error($type . ' type is not implemented yet');
                return;
            }

            //add details to existing IN record of this domain
            $record = $this->handleMultipartRecord($line);

        } else {
            //generate new records
            $record = new DnsRecord();

            if (($type == DnsTypes::IN)) {
                $this->isMultipart = true;
                $record->class = DnsTypes::IN;
                list($type, $params['data']) = explode(' ', $params['data'], 2);
            }

            $rawData = strtok($params['data'], ';');
            $record->data = DnsTypes::generateData($type, $rawData);

            $record->comment = strchr($params['data'], ';') ?? null;
        }

        $record->type = $type;
        $record->name = $name;
        $record->domain_id = $this->domain->id;
        $record->ttl = $this->ttl;

        $record->save();
    }

    public function handleMultipartRecord($line)
    {
        //add details to IN record of this domain
        /**
         * @todo this part should be in repository
         */
        $record = DnsRecord::where('class', DnsTypes::IN)
            ->where('domain_id', $this->domain->id)
            ->first();

        if (!$record) {
            throw new \Exception('no related record found');
        }

        /**
         * @todo this should be handle better and then we can use separate codes instead of using if-else
         */
        $parser = DnsTypes::factory(DnsTypes::SOA);
        $parser->setData(json_decode($record->data));

        list($data, $comment) = explode(';', $line);

        if (strpos($comment, 'serial') !== false) {
            $parser->setSerial($data);

        } elseif (strpos($comment, 'refresh') !== false) {
            $parser->setRefresh($data);

        } elseif (strpos($comment, 'retry') !== false) {
            $parser->setRetry($data);

        } elseif (strpos($comment, 'expire') !== false) {
            $parser->setExpire($data);

        } elseif (strpos($comment, 'TTL') !== false) {
            $parser->setTtl($data);
            $this->ttl = (int)$data;
        }

        $record->data = $parser->toJson();

        return $record;
    }
}
